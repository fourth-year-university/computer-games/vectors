Vector = {}
Vector.__index = Vector

function Vector:create(x, y)
 local vector = {}
 setmetatable(vector, Vector)
 vector.x = x or 0
 vector.y = y or 0
 return vector
end

function Vector:sum(anotherVector)
 local av = Vector:create(0, 0)
 av.x = self.x + anotherVector.x
 av.y = self.y + anotherVector.y
 return av
end

function Vector:sub(anotherVector)
 local av = Vector:create(0, 0)
 av.x = self.x - anotherVector.x
 av.y = self.y - anotherVector.y
 return av
end

function Vector:mul(value)
 local av = Vector:create(0, 0)
 av.x = self.x * value
 av.y = self.y * value
 return av
end

function Vector:div(value)
 local av = Vector:create(0, 0)
 av.x = self.x / value
 av.y = self.y / value
 return av
end

function Vector:mag()
 return math.sqrt(self.x * self.x + self.y * self.y)
end

function Vector:norm()
 local av = Vector:create(0, 0)
 av.x = self.x / self:mag()
 av.y = self.y / self:mag()
 return av
end

function Vector:limit(value)
 local av = self:norm()
 if self:mag() > value then
  av.x = av.x * value
  av.y = av.y * value
 else
  av.x = self.x
  av.y = self.y
 end
 return av
end

function Vector:__tostring()
 return "Vector("..self.x..", "..self.y..") len="..self:mag().."\n"
end