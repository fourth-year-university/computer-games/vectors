 require "vector"
 
 function love.load()
  v1 = Vector:create(0, 10)
  v2 = Vector:create(1, 1)
  print(v1)
  love.window.setMode(400, 400)
  center = Vector:create(200, 200)
 end
 
 
 function love.draw()
  x, y = love.mouse.getPosition()
  mouse = Vector:create(x, y)
  diff = mouse:sub(center)
  --diff = diff:norm():mul(50)
  diff = diff:limit(150)
  mag = diff:mag()
  line_end = center:sum(diff)
  love.graphics.line(center.x, center.y, line_end.x, line_end.y)
  love.graphics.rectangle("fill", 0, 0, mag, 10)
 end